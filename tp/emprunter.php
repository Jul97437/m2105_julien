<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="basic.css" />
    <title></title>
  </head>
  <body>
    <header>
      <a href='index-User.php'>Liste Matériel</a>
      <a href='liste-emprunt.php'>Mes Emprunts</a>
      <?php
        // Verification des variables de session si elles existent
        if (empty($_SESSION["num"]) && empty($_SESSION["mdp"]))
        {
          header('Location: login.php');
        }
        else
        {
          echo"<a href='logout.php'>Logout</a>";
        }
       ?>
    </header>

    <?php
      require("connect.php");

      $sql = "SELECT id_materiel,type,nom FROM materiel WHERE id_materiel =:id ;";
      $req = $bdd->prepare($sql);
      $req->execute(array(
        'id' => $_GET['id']
      ));

      foreach ($req as $val) {
        echo"<h2>Votre emprunt : </h2>";
        echo "Type : ", $val['type'];
        echo"</br></br>";
        echo "Nom : ", $val['nom'];
      }
      echo"</br></br>";

      echo"N° Etudiant : ";
      echo $_SESSION["num"];

      echo"</br></br>";
      setcookie('id',$_GET['id']);
    ?>

      <form action="emprunt-traitement.php?id=<?php echo $_GET['id'] ?>" method="POST">
        Date actuel : <?php echo date('d/m/Y'); ?> <br>
        Date de fin de votre emprunt : <input name="date2" id="date2" type="date">
        </br></br>
        <input type='submit' value='Emprunter'>
      </form>


  </body>
</html>
