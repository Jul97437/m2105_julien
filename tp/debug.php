<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Page de test des variables recues</title>
    </head>
    <body>
        <h1>Debug...</h1>
        <p><b>Parametres tableau $_GET:</b></p>
        <?php
        var_dump($_GET);
        ?>
        <p><b>Parametres tableau $_POST :</b></p>
        <?php
        var_dump($_POST);
        ?>
    </body>
</html>
