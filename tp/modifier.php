<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="basic.css" />
    <title></title>
  </head>
  <body>
    <?php
      // Verification des variables de session si elles existent
      if (!empty($_SESSION["num"]) && !empty($_SESSION["mdp"]))
      {
        header('Location: login.php');
      }
     ?>
    <header>
      <a href='index-Admin.php'>Liste Matériel</a>
      <a href='liste-emprunt-Admin.php'>Liste Emprunts</a>
      <a href='logout.php'>Logout</a>
    </header>

    <?php
      require("connect.php");

      $sql = "SELECT id_materiel,type,nom,emprunt FROM materiel WHERE id_materiel =:id ;";
      $req = $bdd->prepare($sql);
      $req->execute(array(
        'id' => $_GET['id']
      ));

      foreach ($req as $val)
      {
        echo"<h2>Votre modification : </h2>";
        echo "Type : ", $val['type'];
        echo"</br></br>";
        echo "Nom : ", $val['nom'];
        echo"</br></br>";
        echo "Etat : ";
        if ($val['emprunt'] == 0)
        {
          echo" Dispo ";
        }
        else if ($val['emprunt'] == 1)
        {
          echo" Non Dispo ";
        }
        else
        {
          echo" En Réparation ";
        }
      }

      echo"</br></br>";
      echo"</br></br>";

      setcookie('id',$_GET['id']);

      echo "<h2>Modifier matériel :</h2>";

      echo "<form action='modifier-traitement.php' method='POST'>
        Type : <input type='text' name='type'>
        Nom : <input type='text' name='nom'>
        Etat : <input type='int' name='etat'>
        <input type='submit' value='Modifier'>
      </form>";
    ?>



  </body>
</html>
