<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="basic.css" />
    <title>Liste RT</title>
  </head>
  <body>
    <?php
      // Verification des variables de session si elles existent
      if (!empty($_SESSION["num"]) && !empty($_SESSION["mdp"]))
      {
        header('Location: login.php');
      }
     ?>
    <header>
      <a href='index-Admin.php'>Liste Matériel</a>
      <a href='liste-emprunt-Admin.php'>Liste Emprunts</a>
      <a href='logout.php'>Logout</a>
    </header>
    <?php
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Admin~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    require("connect.php");
    echo"</br></br>";
    echo"<h2>Liste du matériel :</h2>";

    $sql = "SELECT id_materiel,type,nom,emprunt FROM materiel ;";
    $req = $bdd->query($sql);

    echo "<table> <tr> <td><b> id </b></td>
                       <td><b> Type </b></td>
                       <td><b> Nom </b></td>
                       <td><b> Etat </b></td>
                       <td><b> Modifier </b></td>
                       <td><b> Init </b></td>
                       <td><b> Supr </b></td>
                  </b></tr>";
        foreach ($req as $row )
        {
            echo "<tr><td>";
            echo $row['id_materiel'];
            echo "</td><td>";
            echo $row['type'];
            echo"</td><td>";
            echo $row['nom'];
            echo"</td><td>";
              if ($row['emprunt'] == 0)
              {
                echo" Dispo ";
              }
              else if ($row['emprunt'] == 1)
              {
                echo" Non Dispo ";
              }
              else
              {
                echo" En Réparation ";
              }
              echo"</td><td>";
              echo"<a href='modifier.php?id=$row[id_materiel]'>Modifier</a>";
              echo"</td><td>";
              echo"<a href='init-traitement.php?id=$row[id_materiel]'>Init</a>";
              echo"</td><td>";
              echo"<a href='sup-traitement.php?id=$row[id_materiel]'>Supr</a>";
            echo "</td></tr>";
        }
        echo "</table>";
        echo "<h2>Ajouter du matériel :</h2>";

        echo "<form action='ajout-traitement.php' method='POST'>
          Id : <input type='int' name='id'>
          Type : <input type='text' name='type'>
          Nom : <input type='text' name='nom'>
          <input type='submit' value='Ajouter materiel'>
        </form>";

    ?>
  </body>
</html>
