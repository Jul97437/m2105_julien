<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="basic.css" />
    <title>Liste RT</title>
  </head>
  <body>
    <header>
      <a href='index-User.php'>Liste Matériel</a>
      <a href='liste-emprunt.php'>Mes Emprunts</a>
      <?php
        // Verification des variables de session existent
        if (empty($_SESSION["num"]) && empty($_SESSION["mdp"]))
        {
          header('Location: login.php');
        }
        else
        {
          echo"<a href='logout.php'>Logout</a>";
        }
      ?>
    </header>

    <?php
      require("connect.php");
      echo"</br></br>";

// Requet pour les emprunts
      $sql = "SELECT id_materiel,num,date_debut,date_fin,rendre FROM emprunt WHERE num = :num ;";
      $req = $bdd->prepare($sql);
      $req->execute(array(
        'num' => $_SESSION["num"]

      ));
      echo"<h2>Votre emprunt : </h2>";

// Tableau des emprunts
      echo "<table> <tr> <td><b> ID du materiel </b></td> <td><b> Date d'emprunt </b></td> <td><b> Date de retour </b></td> <td><b> Rendre </b></td> </tr>";
      foreach ($req as $val)
      {
        echo"<tr><td>";
        echo $val['id_materiel'];
        echo "</td><td>";
        echo $val['date_debut'];
        echo "</td><td>";
        echo $val['date_fin'];
        echo "</td><td>";
      /*  if ($val['date_fin'] == '0')
        {*/
        echo "<a href='rendre-traitement.php?id=$val[id_materiel]'>Rendre</a>";
        //}
        /*else
        {
          echo "En attente"
        }*/
        echo "</td></tr>";
      }

     ?>
  </body>
</html>
